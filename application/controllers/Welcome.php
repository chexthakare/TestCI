<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
    {
            parent::__construct();
            $this->load->Model('WelcomeMdl');
    }
	public function index()
	{
		$data['employee'] =  $this->WelcomeMdl->display();
    	$this->load->view('welcome_message', $data);
	}

	public function add()
	{
		//$this->WelcomeMdl->add();
		$file_name = time().$_FILES["userfile"]['name'];
		$config = array(
            'upload_path' => "./uploads/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => TRUE,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => "768",
            'max_width' => "1024",
            'file_name' => $file_name
        );

        $this->load->library('upload', $config);

        if ($this->upload->do_upload()) 
        {
            $data = array(
                'upload_data' => $this->upload->data()
            );
           
            $imgName = $this->upload->data('file_name');
            $imgPath = $this->upload->data('full_path');

            $data =  array(
            	'fname' => $this->input->post('fname'), 
            	'lname' => $this->input->post('lname'), 
            	'email' => $this->input->post('email'), 
            	'username' => $this->input->post('username'), 
            	'password' => $this->input->post('password'),
            	'imgName' => $imgName,

            );

           	$this->WelcomeMdl->add($data);
           	$data['employee'] =  $this->WelcomeMdl->display();
    		$this->load->view('welcome_message', $data);
        } 
        else
        {
            $error = array(
                'error' => $this->upload->display_errors()
            );
            $this->load->view('welcome_message', $error);
        }
	}

	public function file_view()
    {
        $this->load->view('updateprofile', array('error' => ' '));
    }
    public function do_upload()
    {
        $file_name = time().$_FILES["userfile"]['name'];
        $config = array(
            'upload_path' => "./uploads/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => TRUE,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => "768",
            'max_width' => "1024",
            'file_name' => $file_name
        );
        $this->load->library('upload', $config);
        if ($this->upload->do_upload()) {
            $data = array(
                'upload_data' => $this->upload->data()
            );
           
            $imgName = $this->upload->data('file_name');
            $imgPath = $this->upload->data('full_path');

            $this->load->view('welcome_message', $data);
        } 
        else 
        {
            $error = array(
                'error' => $this->upload->display_errors()
            );
            $this->load->view('welcome_message', $error);
        }
    }

    public function display()
    {
    	$data['employee'] =  $this->WelcomeMdl->display();
    	$this->load->view('welcome_message', $data);
    }

    public function edit ($employeeid)
    {
    	$data['empedit'] = $this->WelcomeMdl->edit($employeeid);
    	$this->load->view('editemp', $data);
    }

    public function update ()
    {
    	$this->WelcomeMdl->update();
    	$data['employee'] =  $this->WelcomeMdl->display();
    	$this->load->view('welcome_message', $data);
    }

    public function profilepic ($employeeid)
    {
    	$data['empedit'] = $this->WelcomeMdl->edit($employeeid);
    	$this->load->view('updateprofile', $data);
    }

    public function updatepic ()
    {
    	$file_name = time().$_FILES["userfile"]['name'];
		$config = array(
            'upload_path' => "./uploads/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => TRUE,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => "768",
            'max_width' => "1024",
            'file_name' => $file_name
        );

        $this->load->library('upload', $config);

        if ($this->upload->do_upload()) 
        {
            $data = array(
                'upload_data' => $this->upload->data()
            );
           
            $imgName = $this->upload->data('file_name');
            //$imgPath = $this->upload->data('full_path');

            $employeeid = $this->input->post('employeeid');
            $data =  array(
            	'imgName' => $imgName
            );

           	$this->WelcomeMdl->updatepic($data,$employeeid);

           	$data['empedit'] = $this->WelcomeMdl->edit($employeeid);
    		$this->load->view('editemp', $data);
        } 
        else
        {
           /* $error = array(
                'error' => $this->upload->display_errors()
            );*/

           	$data['empedit'] = $this->WelcomeMdl->edit($employeeid);
    		$this->load->view('editemp', $data);
        }
    }


}
