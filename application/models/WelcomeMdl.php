<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WelcomeMdl extends CI_Model {

	public function add($data)
	{
		$this->db->insert('employee1',$data);
		echo 'data inserted';
	}

	public function display()
    {
    	return $this->db->get('employee1')->result_array();
    }

    public function edit ($employeeid)
    {
    	return $this->db->get_where('employee1', array('employeeid' => $employeeid))->row();
    }

    public function update ()
    {
    	$employeeid  = $this->input->post('employeeid');

    	$data =  array(
            	'fname' => $this->input->post('fname'), 
            	'lname' => $this->input->post('lname'), 
            	'email' => $this->input->post('email'), 
            	'username' => $this->input->post('username'), 
            	'password' => $this->input->post('password')
            );

    	$this->db->update('employee1', $data, "employeeid = $employeeid");
    }

    public function updatepic ($data,$employeeid)
    {
    	$this->db->update('employee1', $data, "employeeid = $employeeid");
    }
}
