<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/style.css">
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>

<div id="container">
	<h1>Welcome to CodeIgniter!</h1>

	<div id="body">
		<table>
			<!-- <form enctype="multipart/form-data" action="welcome/add" id="myForm" name="myForm"> -->
			<?php echo form_open_multipart('welcome/update',array('id' => 'myForm','name'=>'myForm'))?>
			<tr>
				<td></td>
				<td><a href="<?php echo site_url('welcome/profilepic/').$empedit->employeeid;?>">
					<img style="height:50px; width:50px;" src="<?php echo base_url().'uploads/'.$empedit->imgName;?>"></td>
				</a>

					
			</tr>
			<tr>
				<td>First Name</td><td><input type="text" name="fname" value="<?php echo $empedit->fname;?>">
					<input type="hidden" name="employeeid" value="<?php echo $empedit->employeeid;?>">
				</td>
			</tr>
			<tr>
				<td>Last Name</td><td><input type="text" name="lname" value="<?php echo $empedit->lname;?>"></td>
			</tr>
			<tr>
				<td>Email</td><td><input type="email" name="email" value="<?php echo $empedit->email;?>"></td>
			</tr>
			<tr>
				<td>Username</td><td><input type="text" name="username" value="<?php echo $empedit->username;?>"></td>
			</tr>
			<tr>
				<td>Password</td><td><input type="password" name="password" value="<?php echo $empedit->password;?>"></td>
			</tr>
			
			<tr>
				<td></td><td><input type="submit" id="submit" name="submit" value="submit"></td>
			</tr>
			</form>
		</table>
	</div>

	<p class="footer">If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p>
</div>


</body>
</html>