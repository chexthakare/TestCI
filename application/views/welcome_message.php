<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/style.css">
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>

<div id="container">
	<h1>Welcome to CodeIgniter!</h1>

	<div id="body">
		<table>
			<!-- <form enctype="multipart/form-data" action="welcome/add" id="myForm" name="myForm"> -->
			<?php echo form_open_multipart('welcome/add',array('id' => 'myForm','name'=>'myForm'))?>
			<tr>
				<td>First Name</td><td><input type="text" name="fname"></td>
			</tr>
			<tr>
				<td>Last Name</td><td><input type="text" name="lname"></td>
			</tr>
			<tr>
				<td>Email</td><td><input type="email" name="email"></td>
			</tr>
			<tr>
				<td>Username</td><td><input type="text" name="username"></td>
			</tr>
			<tr>
				<td>Password</td><td><input type="password" name="password"></td>
			</tr>
			<tr>
				<td>Profile Picture</td><td><input type="file" name="userfile"></td>
			</tr>
			<tr>
				<td></td><td><input type="submit" id="submit" name="submit" value="submit"></td>
			</tr>
			</form>
		</table>

		<table>
			<tr>
				<thead>
					<td>UserId</td>
					<td>First Name</td>
					<td>Last Name</td>
					<td>Email</td>
					<td>Username</td>
					<td>Profile Picture</td>
					<td>Action</td>
				</thead>
			</tr>
			<tr>
				<?php foreach ($employee as $value) { ?>
				<tbody>
					<td><?php echo $value['employeeid']; ?></td>
					<td><?php echo $value['fname']; ?></td>
					<td><?php echo $value['lname']; ?></td>
					<td><?php echo $value['email']; ?></td>
					<td><?php echo $value['username']; ?></td>

					<td><img style="height:50px; width:50px;" src="<?php echo base_url().'uploads/'.$value['imgName']; ?>" /></td>
					<td><a href="<?php echo site_url('welcome/edit/').$value['employeeid'];?>">Edit</a></td>
					
				</tbody>
				<?php } ?>
			</tr>
		</table>
		
	</div>

	<p class="footer">If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p>
</div>


<script>   //no need to specify the language
 $(function(){
  $("#sub").click(function(e){  // passing down the event 
  	var formData = new FormData( $("#myForm")[0] );
    $.ajax({
       url:'<?php echo base_url();?>welcome/add',
       type: 'POST',
       //data: $("#myForm").serialize(),
       data : formData,
       async : false,
       cache : false,
       contentType : false,
       processData : false,
       success: function(){
           alert("record added");
       },
       error: function(){
           alert("Fail")
       }
   });
   e.preventDefault(); // could also use: return false;
 });
});
</script>

</body>
</html>